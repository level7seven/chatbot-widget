import React, { PureComponent } from 'react';
import ReactMarkdown from 'react-markdown';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { PROP_TYPES } from 'constants';
import DocViewer from '../docViewer';
import './styles.scss';

class Message extends PureComponent {
  modifyText = (text) => {
    let tempText = text;
    tempText = tempText.replace('oembed', 'iframe')
    tempText = tempText.replace('url', 'src')
    tempText = tempText.replace('watch?v=', 'embed/')
    tempText = tempText.replace('oembed', 'iframe')
    return tempText
  }

  render() {
    const { docViewer, linkTarget } = this.props;
    const sender = this.props.message.get('sender');
    const text = this.props.message.get('text');
    const customCss = this.props.message.get('customCss') && this.props.message.get('customCss').toJS();

    if (customCss && customCss.style === 'class') {
      customCss.css = customCss.css.replace(/^\./, '');
    }

    let textModified = text;

    if (text.includes('<iframe')) {
      textModified = this.modifyText(text)
    } else if (text.includes('<oembed')) {
      textModified = this.modifyText(text)
    }
    return (
      <div
        className={sender === 'response' && customCss && customCss.style === 'class' ?
          `rw-response ${customCss.css}` :
          `rw-${sender}`}
        style={{ cssText: sender === 'response' && customCss && customCss.style === 'custom' ?
          customCss.css :
          undefined }}
      >
        <div
          className="rw-message-text"
        >
          {sender === 'response' ? (
            <div style="white-space: pre-line;" dangerouslySetInnerHTML={{__html: textModified}} />
          ) : (
            <div style="white-space: pre-line;" dangerouslySetInnerHTML={{__html: text}} />
          )}
          {/* {sender === 'response' ? (
            <ReactMarkdown
              className={'rw-markdown'}
              source={text}
              linkTarget={(url) => {
                if (!url.startsWith('mailto') && !url.startsWith('javascript')) return '_blank';
                return undefined;
              }}
              transformLinkUri={null}
              renderers={{
                link: props =>
                  docViewer ? (
                    <DocViewer src={props.href}>{props.children}</DocViewer>
                  ) : (
                    <a href={props.href} target={linkTarget || '_blank'} rel="noopener noreferrer">{props.children}</a>
                  )
              }}
            />
          ) : (
            text
          )} */}
        </div>
      </div>
    );
  }
}

Message.propTypes = {
  message: PROP_TYPES.MESSAGE,
  docViewer: PropTypes.bool,
  linkTarget: PropTypes.string
};

Message.defaultTypes = {
  docViewer: false,
  linkTarget: '_blank'
};

const mapStateToProps = state => ({
  linkTarget: state.metadata.get('linkTarget'),
  docViewer: state.behavior.get('docViewer')
});

export default connect(mapStateToProps)(Message);
