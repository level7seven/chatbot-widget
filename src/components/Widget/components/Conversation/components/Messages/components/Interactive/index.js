import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteLastMessage, emitKgPending, toggleInputDisabled, emitCancel } from 'actions';

import './styles.scss';

class Interactive extends PureComponent {
    constructor(props) {
        super(props);
        this.handleSelectClick = this.handleSelectClick.bind(this);
        this.handleCancelClick = this.handleCancelClick.bind(this);

        this.state = {
            selectedKG: null,
            loading: false,
        };
      }

    handleSelectClick(inputDisabled, loading = null) {
        const {
            handleSelect
        } = this.props;

        const {
            selectedKG
        } = this.state;
    
        if (loading) {
            this.setState({loading});
        }

        handleSelect(selectedKG, inputDisabled);
    }

    handleCancelClick(inputDisabled, emit = true) {
        const {
            handleCancel,
            handleCancelWithoutEmit
        } = this.props;

        if (emit) {
            handleCancel(inputDisabled);
        } else {
            handleCancelWithoutEmit(inputDisabled);
        }
    }

  render() {
      const knowledge_groups = this.props.knowledge_groups;
      console.log('interactive knowledge groups ', knowledge_groups)
    // }
    if (knowledge_groups != null) {
        return (
            <div
                className="rw-response rw-interactive"
            >
                {!this.state.loading && <>
                <div className="rw-markdown">
                    You will be directed to the Agent.<br/>
                    Please select the Knowledge Group below.
                </div>
                <br/>
                <table>
                    {knowledge_groups.knowledge_groups.map((kg) => {
                        return(
                            <tr onClick={() => {
                                // console.log('onclick ', kg, this.state)
                                this.setState({
                                    selectedKG: this.state.selectedKG === kg.id ? null : kg.id,
                                })
                            }}>
                                <td>
                                    <input
                                        type="radio"
                                        value={kg.id}
                                        checked={this.state.selectedKG === kg.id}
                                    />
                                </td>
                                <td>
                                    {kg.name}
                                </td>
                            </tr>
                        )
                    })}
                </table>
                <br/>
                <button disabled={this.state.selectedKG === null} onClick={() => this.handleSelectClick(true, true)} type="button" className="rw-select-button">
                    Select
                </button>
                <button onClick={() => this.handleCancelClick(false, false)} type="button" className="rw-cancel-margin-button">
                    Cancel
                </button>
                </>}
                {this.state.loading && <>
                    <div className="rw-interactive-spinner-wrapper">
                        <span>Please wait for agent</span>
                        <div id="wave">
                            <span className="rw-dot" />
                            <span className="rw-dot" />
                            <span className="rw-dot" />
                        </div>
                    </div>
                    <br/>
                    <button onClick={() => this.handleCancelClick(false)} type="button" className="rw-cancel-button">
                        Cancel
                    </button>
                </>}
                {/* {this.state.loading && <>
                    <div className="rw-interactive-spinner-wrapper">
                        <span>Please wait for agent</span>
                        <div className="rw-interactive-spinner"/>
                    </div>
                    <button onClick={() => this.handleCancelClick(false)} type="button" className="rw-cancel-button">
                        Cancel
                    </button>
                </>} */}
            </div>
        );
    } else {
        return (
            <div
                className="rw-response rw-interactive"
            ></div>
        )
    }
  }
};

Interactive.propTypes = {
  knowledge_groups: PropTypes.arrayOf(PropTypes.object)
};

Interactive.defaultProps = {
    knowledge_groups: [
        {
            id: 6,
            name: 'General'
        },
        {
            id: 67,
            name: 'Naruto'
        },
    ]
};

const mapDispatchToProps = dispatch => ({
    handleSelect: (kgName, disabled) => {
        dispatch(emitKgPending(kgName));
    },
    handleCancel: (disabled) => {
        dispatch(emitCancel());
        dispatch(deleteLastMessage());
        dispatch(toggleInputDisabled(disabled));
    },
    handleCancelWithoutEmit: (disabled) => {
        dispatch(deleteLastMessage());
        dispatch(toggleInputDisabled(disabled));
    },
  });

export default connect(null, mapDispatchToProps)(Interactive);
