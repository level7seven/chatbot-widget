import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteLastMessage, emitKgPending } from 'actions';

import './styles.scss';

class NotificationMessage extends PureComponent {
    // constructor(props) {
    //     super(props);
    //   }

    // handleClick() {
    //     const {
    //       handleSelect
    //     } = this.props;

    //     const {
    //         selectedKG
    //     } = this.state;
    
    //     handleSelect(selectedKG);
    //   }

  render() {
    //   const knowledge_groups = this.props.knowledge_groups;
    //   console.log('interactive knowledge groups ', knowledge_groups)
    // }
    // console.log('this.props', this.props)
    const message = this.props.message.get('text');
    return (
        <div
            className="rw-notification"
        >
            <p>{message}</p>
        </div>
    );
  }
};

// Interactive.propTypes = {
//   knowledge_groups: PropTypes.arrayOf(PropTypes.object)
// };

// Interactive.defaultProps = {
//     knowledge_groups: [
//         {
//             id: 6,
//             name: 'General'
//         },
//         {
//             id: 67,
//             name: 'Naruto'
//         },
//     ]
// };

// const mapDispatchToProps = dispatch => ({
//     handleSelect: (kgName) => {
//         dispatch(deleteLastMessage());
//         dispatch(emitKgPending(kgName));
//     },//   });

// export default connect(null, mapDispatchToProps)(Interactive);
export default NotificationMessage;
